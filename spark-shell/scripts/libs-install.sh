#!/bin/bash

mkdir -p /opt/libs
cd /opt/libs

#wget http://search.maven.org/remotecontent?filepath=org/apache/ivy/ivy/2.4.0/ivy-2.4.0.jar -O ivy-2.4.0.jar
#wget http://search.maven.org/remotecontent?filepath=/com/datastax/spark/spark-cassandra-connector_2.11/1.2.0-alpha3/spark-cassandra-connector_2.11-1.2.0-alpha3.jar -O spark-cassandra-connector_2.11-1.2.0-alpha3.jar
 
#ivy () { java -jar ivy-2.4.0.jar -dependency $* -retrieve "[artifact]-[revision](-[classifier]).[ext]"; }
 
#ivy org.apache.cassandra cassandra-thrift 2.1.3
#ivy com.datastax.cassandra cassandra-driver-core 2.1.4
#ivy joda-time joda-time 2.7
#ivy org.joda joda-convert 1.7

pip install cassandra-driver

echo "Building Spark-Cassandra-Connector"
cd /tmp
git clone https://github.com/datastax/spark-cassandra-connector
cd spark-cassandra-connector
sbt -Dscala-2.11=true doc
sbt -Dscala-2.11=true package
sbt -Dscala-2.11=true assembly
cp spark-cassandra-connector/spark-cassandra-connector/target/scala-2.11/spark-cassandra-connector-assembly*.jar /opt/libs/
cd ..
rm -rf spark-cassandra-connector
echo "spark.executor.extraClassPath  /opt/libs/" >> /opt/spark/conf/spark-defaults.conf