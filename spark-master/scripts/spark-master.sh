#!/bin/bash
. /usr/local/sbin/spark-common

## Cluster Zookeper specific

if [ -n $SPARK_DEPLOY_ZOOKEPER_URL ]; then
	
	SPARK_DEPLOY_RECOVERYMODE=${SPARK_DEPLOY_RECOVERYMODE:-"ZOOKEEPER"}
	SPARK_DEPLOY_ZOOKEPER_DIR=${SPARK_DEPLOY_ZOOKEPER_DIR:-"spark"}

	export SPARK_DAEMON_JAVA_OPTS="${SPARK_DAEMON_JAVA_OPTS} \
	 -Dspark.deploy.zookeeper.url=\"${SPARK_DEPLOY_ZOOKEPER_URL}\" \
	 -Dspark.deploy.recoveryMode=\"${SPARK_DEPLOY_RECOVERYMODE}\" \
	 -Dspark.deploy.zookeeper.dir=\"/${SPARK_DEPLOY_ZOOKEPER_DIR}\""
fi

# Avoid the default Docker behavior of mapping our IP address to an unreachable host name
umount /etc/hosts

/opt/spark/sbin/start-master.sh
